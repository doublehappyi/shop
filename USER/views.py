from django.shortcuts import render, redirect

# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
import pdb;


def register(request):
    return render(request, 'USER/register.html')


def registerUser(request):
    POST = request.POST
    username = POST["username"]
    password = POST["password"]
    email = POST["email"]

    new_user = User.objects.create_user(username=username, password=password, email=email)
    return HttpResponseRedirect('registerSuccess')


@login_required
def registerSuccess(request):
    return render(request, "USER/registerSuccess.html")


def login(request):
    request.session['next'] = request.META.get('HTTP_REFERER', '/')
    pdb.set_trace();
    if request.user.is_authenticated():
        return redirect("/ORDER/goods_list")
    else:
        return render(request, "USER/login.html")


def loginUser(request):
    POST = request.POST

    try:
        username = POST["username"]
        password = POST["password"]
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            next = request.GET['next']
            if not next:
                next = "/ORDER/goods_list"
            return HttpResponseRedirect(request.session['next'])
        else:
            return render(request, "USER/loginFailed.html")
    except:
        pass

    return render(request, "USER/login.html")


@login_required
def loginSuccess(request):
    return render(request, "USER/loginSuccess.html")


@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("login")



