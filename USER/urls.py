__author__ = 'YI'

from django.conf.urls import patterns, url
from USER import views

urlpatterns = patterns('',
                       url(r"^register$", views.register, name='register'),
                       url(r"^registerUser$", views.registerUser, name='registerUser'),
                       url(r"^registerSuccess$", views.registerSuccess, name='registerSuccess'),
                       url(r"^login$", views.login, name='login'),
                       url(r"^loginUser$", views.loginUser, name='loginUser'),
                       url(r"^loginSuccess$", views.loginSuccess, name='loginSuccess'),
                       url(r"^logout$", views.logout, name='logout'),
)