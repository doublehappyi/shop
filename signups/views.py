from django.shortcuts import render, render_to_response, RequestContext
from django.http import HttpResponse
# Create your views here.

def signup(request):
    return render_to_response("signup.html",
                              locals(),
                              context_instance=RequestContext(request)
    )
    #return HttpResponse("aa")