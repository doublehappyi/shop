from django.conf.urls import patterns, include, url

from django.contrib import admin
from shop import views

admin.autodiscover()
from signups import views

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'shop.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^USER/', include('USER.urls')),
                       url(r'^ORDER/', include('ORDER.urls')),
                       url(r'^$', views.signup, name='signup')
)
