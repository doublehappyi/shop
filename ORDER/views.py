# coding=utf8
from django.shortcuts import render
from ORDER.models import Goods, Carts
from django.utils import simplejson
from django.contrib.auth.decorators import login_required
# Create your views here.
import sys

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

from django.http import HttpResponse


@login_required
def cart_list(request):
    user = request.user
    carts = Carts.objects.filter(user_id=user.id)
    context = {"carts": carts}
    return render(request, 'ORDER/cart_list.html', context)


def goods_list(request):
    goods = Goods.objects.all()
    context = {'goods': goods}
    return render(request, 'ORDER/goods_list.html', context)


@login_required
def add_to_cart(request):
    user = request.user
    goods_id = request.POST["goods_id"]

    # target = Carts.objects.filter(goods_id=goods_id)[0]
    targetList = Carts.objects.filter(goods_id=goods_id, user_id=user.id)
    if len(targetList) > 0:
        targetList[0].number += 1
        targetList[0].save()
    else:
        Carts.objects.create(title=Goods.objects.get(pk=goods_id).title, goods_id=goods_id, user_id=user.id)
    data = {"success": True}
    # ensure_ascii=False用于处理中文
    return HttpResponse(simplejson.dumps(data, ensure_ascii=False))