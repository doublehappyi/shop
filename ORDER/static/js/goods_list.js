console.log('goods_list');

$(function(){
    $(".add-to-cart").click(function(){
        var $thisBtn = $(this),
            $thisId = $thisBtn.find(".goods-id"),
            $csrf = $thisBtn.find("[name=csrfmiddlewaretoken]");

        $.ajax("/ORDER/add_to_cart",{
            type:"POST",
            dataType:"json",
            data:{
                goods_id:$thisId.val(),
                csrfmiddlewaretoken:$csrf.val()
            },
            success:function(json){
                alert("已加入购物车");
            }
        });
    });
});
