__author__ = 'YI'

from django.conf.urls import patterns, url
from ORDER import views
urlpatterns = patterns('',
                       url(r'^cart_list$',views.cart_list,name='cart_list'),
                       url(r'^goods_list$',views.goods_list,name='goods_list'),
                       url(r'^add_to_cart$',views.add_to_cart,name='add_to_cart'),
)