from django.db import models

# Create your models here.
class Goods(models.Model):
    image = models.CharField(max_length=200)
    title = models.CharField(max_length=20)
    desc = models.CharField(max_length=200)
    price = models.IntegerField(default=0)

    def __unicode__(self):
        return self.title


class Carts(models.Model):
    title = models.CharField(max_length=50)
    number = models.IntegerField(default=1)
    goods_id = models.IntegerField(default=1)
    user_id = models.IntegerField(default=2)

    def __unicode__(self):
        return self.title


